﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using salesForce2.iOS;
using salesForce2.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]

namespace salesForce2.iOS
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;
            Control.TextAlignment = UITextAlignment.Center;

            //Control.Font = UIFont.FromName("Helvetica bold", 14f);
            //Control.Font = UIFont.
            Control.Font = UIFont.BoldSystemFontOfSize(14);
            Control.Font = UIFont.SystemFontOfSize(16);
            //Control.Font = UIFont.SystemFontOfSize(14);
            //var view = e.NewElement as Picker;
            //this.Control.BorderStyle = UITextBorderStyle.None;
        }
    }
}