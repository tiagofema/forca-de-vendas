﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using salesForce2.Methods;
using Xamarin.Forms;

namespace salesForce2.Behaviors
{
    public class PlacaBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += OnTextChanged;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnTextChanged;

            base.OnDetachingFrom(bindable);
        }

        private static void OnTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = (Entry)sender;

            entry.Text = FormataPlaca(entry.Text);
        }

        private static string FormataPlaca(string input)
        {
            var inputRegex = new Regex(@"^[a-zA-Z]{3}\-\d{4}$");
            if (!inputRegex.IsMatch(input))
            {

                input = VerificadoresDocumentos.LimpezaGeral(input);
                return input.Length <= 3 ? input : $"{input.Substring(0, 3)}-{input.Substring(3)}";
            }
            return input;
        }
    }
}
