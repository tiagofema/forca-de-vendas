﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace salesForce2.Behaviors
{
    public class NumeroBehavior : Behavior<Entry>
    {

            static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(NumeroBehavior), false);

            public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

            public bool IsValid
            {
                get => (bool)GetValue(IsValidProperty);
                private set => SetValue(IsValidPropertyKey, value);
            }

            protected override void OnAttachedTo(Entry bindable)
            {
                bindable.TextChanged += bindable_TextChanged;
            }

        public void bindable_TextChanged(object sender, TextChangedEventArgs e)
            {
                IsValid = double.TryParse(e.NewTextValue, out _);
                ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;
            }
            protected override void OnDetachingFrom(Entry bindable)
            {
                bindable.TextChanged -= bindable_TextChanged;
            }
        }
    }

