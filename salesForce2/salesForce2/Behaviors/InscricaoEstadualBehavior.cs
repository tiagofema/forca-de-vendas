﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using salesForce2.Methods;
using Xamarin.Forms;

namespace salesForce2.Behaviors
{
    public class InscricaoEstadualBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += OnTextChanged;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnTextChanged;

            base.OnDetachingFrom(bindable);
        }

        private static void OnTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = (Entry)sender;

            entry.Text = FormataDocumento(entry.Text);
        }

        private static string FormataDocumento(string digits)
        {
            if (VerificadoresDocumentos.IsDigitsOnly(digits))
            {
                if (digits.Length <= 8)
                    return digits;
                if (digits.Length <= 9)
                    return
                        $"{digits.Substring(0, 2)}.{digits.Substring(2, 3)}.{digits.Substring(5, 3)}-{digits.Substring(8)}";
                if (digits.Length <= 12)
                    return
                        $"{digits.Substring(0, 3)}.{digits.Substring(3, 3)}.{digits.Substring(6, 3)}-{digits.Substring(9)}";
                return
                    $"{digits.Substring(0, 2)}.{digits.Substring(2, 3)}.{digits.Substring(5, 3)}/{digits.Substring(8, 4)}-{digits.Substring(12)}";
            }
            else return digits;
        }
    }
}
