﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace salesForce2.Models
{
    public class MasterPageItem
    {
        public string Titulo { get; set; }
        public string NomeImagem { get; set; }
    }
}
