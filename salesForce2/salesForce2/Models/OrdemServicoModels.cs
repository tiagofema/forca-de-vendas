﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace salesForce2.Models
{
    [Preserve(AllMembers = true)]
    public class OrdemServicoModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public int FuncionarioId { get; set; }
        public int CarroFipeId { get; set; }
        public int CombustivelId { get; set; }
        public int ClienteId { get; set; }
        public int NumeroOrcamento { get; set; }
        public int Ano { get; set; }
        public decimal Quilometragem { get; set; }
        public string Observacao { get; set; }
        public string Defeitos { get; set; }
        public string Avarias { get; set; }
        public string Placa { get; set; }
        public int WorkMotorId { get; set; }
        public DateTime DataEmissao { get; set; }
        public string Cor { get; set; }
    }
}
