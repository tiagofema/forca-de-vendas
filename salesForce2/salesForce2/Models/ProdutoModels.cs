﻿using System;
using System.Numerics;
using Xamarin.Forms.Internals;

namespace salesForce2.Models
{
    [Preserve(AllMembers = true)]
    public class ProdutoModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public int MarcaId { get; set; }
        public int FamiliaId { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string ReferenciaFabricante { get; set; }
        public string Modelo { get; set; }
        public string Tamanho { get; set; }
        public decimal Peso { get; set; }
        public string Local { get; set; }
        public decimal ValorVenda { get; set; }
        public string Unidade { get; set; }
        public DateTime DataModificacao { get; set; }
        public double Quantidade { get; set; }
        public string Aplicacao { get; set; }
        public string CodigoReferencia { get; set; }

        public string FamiliaDescricao { get; set; }
        public string MarcaDescricao { get; set; }
    }
}
