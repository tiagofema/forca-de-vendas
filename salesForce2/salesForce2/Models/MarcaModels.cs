﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace salesForce2.Models
{
    [Preserve(AllMembers = true)]
    public class MarcaModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public string Codigo { get; set; }
        public string Tipo { get; set; }
        public string Descricao { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}
