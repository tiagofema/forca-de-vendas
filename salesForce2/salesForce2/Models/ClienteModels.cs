﻿using System;
using Xamarin.Forms.Internals;

namespace salesForce2.Models
{
    [Preserve(AllMembers = true)]
    public class ClienteModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CnpjCpf { get; set; }
        public string InscricaoEstadual { get; set; }
        public string Endereco { get; set; }
        public int Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Cep { get; set; }
        public int Ddd { get; set; }
        public string Telefone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        //public string Documento { get; set; }

        public int Codigo { get; set; }
        public int CelularDdd { get; set; }
        public string Celular { get; set; }
        public DateTime DataNascimento { get; set; }
        public char Sexo { get; set; }


    }
}
