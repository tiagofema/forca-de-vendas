﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace salesForce2.Models
{
    public class ClienteSimplesModels
    {
        public string Placa { get; set; }
        public string Ano { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Cor { get; set; }
        public string Nome { get; set; }
    }
}
