﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace salesForce2.Models
{
    [Preserve(AllMembers = true)]
    public class CombustivelModels
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
    }
}
