﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace salesForce2.Http
{
    public static class Client
    {
        //public static HttpClient Http => new HttpClient {BaseAddress = new Uri("http://192.168.0.115:62838/")};
        public static HttpClient Http => new HttpClient { BaseAddress = new Uri("http://201.77.177.34/") };
        //public static HttpClient Http => new HttpClient { BaseAddress = new Uri("http://localhost:62838/")};
        //public static HttpClient Endereco => new HttpClient {BaseAddress = new Uri("http://api.postmon.com.br/v1/cep/")};
        public static HttpClient Endereco => new HttpClient { BaseAddress = new Uri("https://viacep.com.br/ws/") };

        public static HttpClient MegaLaudo => new HttpClient
        {
            BaseAddress = new Uri("http://201.77.177.42/ApiMegaLaudo/")
        };
    }
}
