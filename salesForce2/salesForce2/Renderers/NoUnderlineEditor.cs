﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace salesForce2.Renderers
{
    [Preserve(AllMembers = true)]
    public class NoUnderlineEditor : Editor
    {
        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create<NoUnderlineEditor, string>(view => view.Placeholder, String.Empty);

        public NoUnderlineEditor()
        {
        }

        public string Placeholder
        {
            get => (string)GetValue(PlaceholderProperty);

            set => SetValue(PlaceholderProperty, value);
        }
    }
}
