﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using salesForce2.Http;
using salesForce2.Models;

namespace salesForce2.Methods
{
    public static class SerializeAppDataMethods
    {
        public static async Task<string> SerializaAppData(AppData appData)
        {
            appData.CurrentCliente = new ClienteModels();
            appData.CurrentClienteList = new List<ClienteModels>();
            appData.CurrentProdutoList = new List<ProdutoModels>();
            //AppData.TodosProdutosCarregados= false;
            //AppData.TodosClientesCarregados = false;
            appData.CurrentProduto = new ProdutoModels();
            await DesativaFuncionario(appData);
            //await SavePropertiesAsync();
            //var a = JsonConvert.SerializeObject(AppData);
            return JsonConvert.SerializeObject(appData);
        }

        public static async Task DesativaFuncionario(AppData appData)
        {
            var values = new Dictionary<string, string>
            {
                { "funcionarioId", appData.CurrentFuncionario.Id.ToString() }
            };
            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/DesativaFuncionario", content);
            await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> VerificaFuncionario(AppData appData)
        {
            var values = new Dictionary<string, string>
            {
                { "funcionarioId", appData.CurrentFuncionario.Id.ToString() }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/RelogaFuncionario", content);
            appData.BuscaString = "";
            return await response.Content.ReadAsStringAsync();
        }

    }
}
