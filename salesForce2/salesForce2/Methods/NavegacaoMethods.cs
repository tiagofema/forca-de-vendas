﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using salesForce2.Views;
using Xamarin.Forms;

namespace salesForce2.Methods
{
    public static class NavegacaoMethods
    {
        public static void NavegaMenuPrincipal()
        {
            var paginaFuncionario = new NavigationPage(new MenuPrincipal2())
            {
                BarBackgroundColor = Color.FromHex("#223c74"),
                BarTextColor = Color.White
            };
            var rootPage = new MasterDetailBase
            {
                Master = new MenuLateral(),
                Detail = paginaFuncionario
            };
            Application.Current.MainPage = rootPage;
        }
    }
}
