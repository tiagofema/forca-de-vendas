﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace salesForce2.Views
{

    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailBase : MasterDetailPage
    {
        public MasterDetailBase()
        {
            InitializeComponent();
            //MasterBehavior = MasterBehavior.Popover;
            NavigationPage.SetHasNavigationBar(this, false);
            App.MasterDetail = this;
        }
    }
}