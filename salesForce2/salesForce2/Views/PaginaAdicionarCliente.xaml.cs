﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using salesForce2.Http;
using salesForce2.Methods;
using salesForce2.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace salesForce2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaAdicionarCliente : ContentPage
    {
        public PaginaAdicionarCliente()
        {
            InitializeComponent();
            CarregaClienteMegaLaudo();
            //InicializaCampo();
        }

        //private void InicializaCampo()
        //{
        //    var appData = (AppData)BindingContext;
        //    try
        //    {
        //        NroDocumento.Text = appData.CurrentCliente.CnpjCpf;
        //    }
        //    catch
        //    {
        //        //
        //    }
        //}

        private async void CarregaClienteMegaLaudo()
        {
            var appData = (AppData)BindingContext;
            if (!appData.InsercaoClienteOs) return;
            CarregandoMegaLaudoSwitch(true);
            try
            {
                var a = await EnvioMethods.RetornoMegaLaudo(appData.CurrentVeiculoCliente.Placa);
                var clienteSimples = JsonConvert.DeserializeObject<ClienteSimplesModels>(a);
                Nome.Text = clienteSimples.Nome;
                appData.CurrentVeiculoCliente.Placa = clienteSimples.Placa.ToUpper();
                appData.CurrentVeiculoCliente.Cor = clienteSimples.Cor.ToUpper();
                appData.CurrentVeiculoCliente.Descricao = clienteSimples.Modelo;
                //appData.CurrentVeiculoCliente.Ano = int.Parse(clienteSimples.Ano);
                appData.CurrentVeiculoCliente.Ano = (clienteSimples.Ano);
            }
            catch (Exception error)
            {
                CarregandoMegaLaudoSwitch(false);
                //DisplayAlert("Erro", error.Message, "Ok");
            }
            CarregandoMegaLaudoSwitch(false);
        }

        private async void Avancar_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            //----------------------------------------
            //Nome.Text = "FELIPE NEGRI";
            //NroDocumento.Text = "37311225884";
            //Telefone.Text = "1938344187";
            //Numero.Text = "259";
            //Endereco.Text = "Rua João Tibiriçá Piratininga";
            //Complemento.Text = "";
            //Cep.Text = "13330-450";
            //Email.Text = "tfmartinfox@hotmail.com";
            //Cidade.Text = "Indaiatuba";
            //Bairro.Text = "Jardim Pau Preto";
            //Uf.Text = "SP";
            //NroInscricaoEstadual.Text = "2481035810";
            //Celular.Text = "19996750555";
            //DataNascimento.Text = "27/12/1993";
            //Sexo.Text = "M";
            //----------------------------------------
            try
            {
                if (string.IsNullOrEmpty(Nome.Text))
                {
                    await DisplayAlert("Erro", "Por favor preencha nome do cliente.", "Ok");
                    return;
                }
                appData.CurrentCliente = new ClienteModels();
                appData.CurrentCliente.RazaoSocial =
                    VerificadoresDocumentos.LimpezaGeral(VerificadoresDocumentos.RemoverAcentos(Nome.Text).ToUpper());
                appData.CurrentCliente.NomeFantasia =
                    VerificadoresDocumentos.LimpezaGeral(VerificadoresDocumentos.RemoverAcentos(Nome.Text).ToUpper());
            }
            catch (Exception erroNome)
            {
                await DisplayAlert("Erro",
                    "O nome do cliente contém caracteres inválidos. Por favor digite-o somente com letras.", "Ok");
                return;
            }
            try
            {
                if (string.IsNullOrEmpty(DataNascimento.Text))
                {
                    await DisplayAlert("Erro", "Por favor digite a data de nascimento do cliente.", "Ok");
                    return;
                }
                
                appData.CurrentCliente.DataNascimento =
                    DateTime.ParseExact(DataNascimento.Text, "dd/MM/yyyy", null);
                //var dateTime = DateTime.ParseExact(DataNascimento.Text, "d",CultureInfo.CurrentCulture);
            }
            catch (Exception erro)
            {
                //await DisplayAlert("Erro", "ME FALA O ERRO" + erro.Message, "Ok");
                await DisplayAlert("Erro",
                    "Problemas em reconhecer a data. Digite somente números e no formato DIA/MÊS/ANO", "Ok");
                return;
            }
            try
            {
                if (string.IsNullOrEmpty(Sexo.Text))
                {
                    await DisplayAlert("Erro", "Por favor escolha um sexo para o cliente", "Ok");
                    return;
                }
                appData.CurrentCliente.Sexo = Sexo.Text[0];
            }
            catch
            {
                await DisplayAlert("Erro", "Por favor escolha um sexo para o cliente e tente novamente.", "Ok");
                return;
            }
            try
            {
                if (VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text).Length != 11 &&
                    VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text).Length != 14 ||
                    string.IsNullOrEmpty(NroDocumento.Text))
                {
                    await DisplayAlert("Erro",
                        "O número do documento não contem a quantidade correta de digitos.", "Ok");
                    return;
                }
            }
            catch
            {
                await DisplayAlert("Erro", "O número do documento não pode ser deixado vazio.", "Ok");
                return;
            }
            if (NroDocumento.Text.Length == 18)
            {
                if (!VerificadoresDocumentos.IsCnpj(VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text)))
                {
                    await DisplayAlert("Erro", "O número de CNPJ é inválido. Verifique os digitos e tente novamente.",
                        "Ok");
                    return;
                }
            }
            else
            {
                if (!VerificadoresDocumentos.IsCpf(VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text)))
                {
                    await DisplayAlert("Erro", "O número de CPF é inválido. Verifique os digitos e tente novamente.",
                        "Ok");
                    return;
                }
            }
            appData.CurrentCliente.CnpjCpf = VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text);
            if (string.IsNullOrEmpty(NroInscricaoEstadual.Text) || NroInscricaoEstadual.Text.Length < 3)
            {
                await DisplayAlert("Erro", "O número de RG/Inscrição Estadual é obrigatório.", "Ok");
                return;
            }
            appData.CurrentCliente.InscricaoEstadual =
                VerificadoresDocumentos.LimpezaPlaca(NroInscricaoEstadual.Text.ToUpper());
            appData.CurrentCliente.OficinaId = appData.CurrentOficina.Id;
            try
            {
                appData.CurrentCliente.Email = Email.Text.ToUpper();
            }
            catch
            {
                await DisplayAlert("Erro", "E-Mail inválido.", "Ok");
                return;
            }
            try
            {
                if (VerificadoresDocumentos.LimpezaNumeros(Telefone.Text).Length < 10)
                {
                    await DisplayAlert("Erro",
                        "Número de telefone inválido. Digite o DDD seguido pelo número da linha.", "Ok");
                    return;

                }
                appData.CurrentCliente.Ddd =
                    int.Parse(VerificadoresDocumentos.LimpezaNumeros(Telefone.Text).Substring(0, 2));
                appData.CurrentCliente.Telefone = VerificadoresDocumentos.LimpezaNumeros(Telefone.Text).Substring(2);
            }
            catch
            {
                await DisplayAlert("Erro", "Por favor, digite um número de telefone.", "Ok");
                return;
            }
            try
            {
                if (VerificadoresDocumentos.LimpezaNumeros(Celular.Text).Length < 10)
                {
                    await DisplayAlert("Erro",
                        "Número de celular inválido. Digite o DDD seguido pelo número da linha.", "Ok");
                    return;

                }
                appData.CurrentCliente.CelularDdd =
                    int.Parse(VerificadoresDocumentos.LimpezaNumeros(Celular.Text).Substring(0, 2));
                appData.CurrentCliente.Celular = VerificadoresDocumentos.LimpezaNumeros(Celular.Text).Substring(2);
            }
            catch
            {
                await DisplayAlert("Erro", "Por favor, digite um número de celular.", "Ok");
                return;
            }
            if (string.IsNullOrEmpty(Cep.Text) || Cep.Text.Length < 8)
            {
                await DisplayAlert("Erro", "Por favor, digite um CEP válido", "Ok");
                return;
            }
            if (string.IsNullOrEmpty(Endereco.Text))
            {
                await DisplayAlert("Erro", "Por favor, utilize a busca de CEP para localizar um endereço válido.",
                    "Ok");
                return;
            }
            appData.CurrentCliente.Bairro = Bairro.Text.ToUpper();
            if (string.IsNullOrEmpty(Cidade.Text) || string.IsNullOrEmpty(Bairro.Text) || string.IsNullOrEmpty(Uf.Text))
            {
                BuscaCep_OnClicked(new object(), new EventArgs());
            }
            appData.CurrentCliente.Cep = VerificadoresDocumentos.LimpezaNumeros(Cep.Text);
            appData.CurrentCliente.Cidade =
                VerificadoresDocumentos.LimpezaGeral(
                    VerificadoresDocumentos.RemoverAcentos(Cidade.Text.ToUpper().Replace("\'", " ")));
            appData.CurrentCliente.Endereco =
                VerificadoresDocumentos.LimpezaGeral(
                    VerificadoresDocumentos.RemoverAcentos(Endereco.Text.ToUpper().Replace("\'", " ")));
            try
            {
                appData.CurrentCliente.Numero = int.Parse(VerificadoresDocumentos.LimpezaNumeros(Numero.Text));
            }
            catch
            {
                await DisplayAlert("Erro", "Digite somente números no campo número", "Ok");
                return;
            }
            appData.CurrentCliente.Complemento = string.IsNullOrEmpty(Complemento.Text)
                ? "SEM COMPLEMENTO"
                : VerificadoresDocumentos.LimpezaGeral(Complemento.Text.ToUpper().Replace("\'", " "));
            appData.CurrentCliente.Estado = Uf.Text;
            appData.CurrentCliente.Codigo = 0;
            appData.AdicionarCliente = true;
            if (!appData.InsercaoClienteOs)
            {
                var resposta = await DisplayAlert("", "Deseja adicionar um veículo para este cliente?", "Sim", "Não");
                if (resposta)
                {
                    await Navigation.PushAsync(new PaginaAdicionarVeiculo());
                    return;
                }
            }
            else
            {
                await Navigation.PushAsync(new PaginaAdicionarVeiculo());
                return;
            }
            try
            {
                var resposta2 = await EnvioMethods.EnvioCliente(appData.CurrentCliente);
                if (resposta2 == "true")
                {
                    await DisplayAlert("Sucesso", "O cliente foi gravado com sucesso", "Ok");
                    appData.AdicionarCliente = false;
                    NavegacaoMethods.NavegaMenuPrincipal();
                }
                else
                {
                    appData.AdicionarCliente = false;
                    await DisplayAlert("Erro",
                        "Houve algum problema com a gravação do cliente. Verifique os dados e tente novamente.", "Ok");
                    return;
                }
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro",
                    "Houve um erro na gravação do cliente. Verifique sua conexão com a internet e tente novamente.",
                    "Ok");
                return;
            }
        }

        private async void BuscaCep_OnClicked(object sender, EventArgs e)
        {
            CarregandoCepSwitch(true);
            try
            {
                var response = await Client.Endereco.GetAsync(Cep.Text + "/json");
                var resultado = await response.Content.ReadAsStringAsync();
                var endereco = JsonConvert.DeserializeObject<EnderecoModels>(resultado);
                if (string.IsNullOrEmpty(endereco.Erro))
                {
                    Endereco.Text = VerificadoresDocumentos.RemoverAcentos(endereco.Logradouro);
                    Bairro.Text = VerificadoresDocumentos.RemoverAcentos(endereco.Bairro);
                    Cidade.Text = VerificadoresDocumentos.RemoverAcentos(endereco.Localidade);
                    Uf.Text = endereco.Uf;
                    CarregandoCepSwitch(false);
                }
                else
                {
                    await DisplayAlert("Erro", "CEP não encontrado. Verifique os numero e tente novamente.", "Ok");
                    CarregandoCepSwitch(false);
                }
            }
            catch
            {
                await DisplayAlert("Erro", "Nenhum CEP encontrado. Verifique os digitos e tente novamente.", "Ok");
                CarregandoCepSwitch(false);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {

            }
        }

        private async void Sexo_OnFocused(object sender, FocusEventArgs e)
        {
            SexoPicker.SelectedIndex = -1;
            Sexo.Unfocus();
            SexoPicker.Focus();
            while (SexoPicker.SelectedIndex == -1)
            {
                await Task.Delay(25);
            }
            Sexo.Text = SexoPicker.SelectedItem.ToString();
        }

        private void CarregandoCepSwitch(bool entry)
        {
            CarregandoCep.IsRunning = entry;
            CarregandoCep.IsVisible = entry;
        }

        private void CarregandoMegaLaudoSwitch(bool entry)
        {
            CarregandoMegaLaudoTexto.IsVisible = entry;
            CarregandoMegaLaudo.IsRunning = entry;
            CarregandoMegaLaudo.IsVisible = entry;
        }
    }
}