﻿using System.Linq;
using salesForce2.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace salesForce2.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaPeca : ContentPage
    {
        //public PaginaPeca(ProdutoModels produto)
        public PaginaPeca()
        {
            InitializeComponent();
            //BindingContext = produto;
            Title = "";
            Titulador();
        }

        private void Titulador()
        {
            var appData = (AppData)BindingContext;
            if (string.IsNullOrEmpty(appData.CurrentProduto.Descricao)) return;
            var split = appData.CurrentProduto.Descricao.Split(' ');
            var i = 0;
            var contador = split.Count();
            if (contador >= 5)
            {
                while (i < 5 && Title.Length < 18)
                {
                    Title = Title + " " + split[i];
                    i++;
                }
            }
            else
            {
                while (i < contador && Title.Length < 15)
                {
                    if (i > 2 && split[i].Length < 3)
                    {
                        i++;
                    }
                    else
                    {
                        Title = Title + " " + split[i];
                        i++;
                    }
                }
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {

            }
        }
    }
}