﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using salesForce2.Http;
using salesForce2.Methods;
using salesForce2.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace salesForce2.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class MenuPrincipal2 : ContentPage
    {
        public bool Clique;
        public MenuPrincipal2()
        {
            InitializeComponent();
            var appData = (AppData) BindingContext;
            //Task.Run(async () => { await CarregarNumeroOrcamento(); });
            CarregarNumeroOrcamento();
            Task.Run(async () => { appData.Combustiveis = await EnvioMethods.RetornaCombustiveis(); }).Wait();
            //NavigationPage.SetTitleIcon(this, "workmotorlogo2.png");
        }

        private async Task VerificaOrdemServico()
        {
            var appData = (AppData)BindingContext;
            if (!appData.InsercaoClienteOs) return;
            appData.InsercaoClienteOs = false;
            //await Navigation.PushAsync(new PaginaOrdemServico());
            return;
        }

        private async void BotaoConsultaTodasPecas(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            appData.Aplicacoes = false;
            if (Clique) return;
            this.Title = "Menu";
            Clique = true;
            await Navigation.PushAsync(new PaginaTodasPecas());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            //ImagensSwitch(true);
            Clique = false;
        }

        private async void BotaoAplicacoes(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            appData.Aplicacoes = true;
            if (Clique) return;
            this.Title = "Menu";
            Clique = true;
            await Navigation.PushAsync(new PaginaTodasPecas());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            //ImagensSwitch(true);
            Clique = false;

        }

        private async void BotaoConsultaTodosClientes(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            if (Clique) return;
            Clique = true;
            this.Title = "Menu";
            //ImagensSwitch(false);
            await Navigation.PushAsync(new PaginaTodosClientes());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            Clique = false;
            //ImagensSwitch(true);
        }

        private async void BotaoOrdemServico(object sender, EventArgs e)
        {
            //ImagensSwitch(false);
            if (Clique) return;
            Clique = true;
            var appData = (AppData)BindingContext;
            appData.Avarias = "";
            appData.Defeitos = "";
            appData.Observacao = "";
            this.Title = "Menu";
            appData.CurrentCliente = new ClienteModels();
            appData.CurrentOrdemServico = new OrdemServicoModels();
            await Navigation.PushAsync(new PaginaOrdemServico());
            Clique = false;
            //ImagensSwitch(true);
        }

        private async Task CarregarNumeroOrcamento()
        {
            var appData = (AppData)BindingContext;
            appData.Logado = true;
            appData.InsercaoClienteOs = false;
            appData.CurrentCliente = new ClienteModels();
            appData.CurrentClienteVeiculo = new List<ClienteVeiculoModels>();
            Application.Current.Properties["appData"] = JsonConvert.SerializeObject(appData);
            await Application.Current.SavePropertiesAsync();
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "funcionarioId", appData.CurrentOficina.Id.ToString() }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await Client.Http.PostAsync("home/RetornarTodasOrdensServico", content);
                //var response = await Client.Http.GetAsync("home/RetornarTodasOrdensServico?id=" + appData.CurrentOficina.Id);
                var resultado = await response.Content.ReadAsStringAsync();
                var listaOrdemServicos = JsonConvert.DeserializeObject<List<OrdemServicoModels>>(resultado);
                appData.NumeroOrcamento = listaOrdemServicos.Count;
            }
            catch (Exception)
            {
                //
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.Title = "WorkMotorApp";
            var appData = (AppData) BindingContext;

            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = true;
            }
            catch (Exception error)
            {
                //DisplayAlert("Erro", error.Message, "Ok");
            }
            /*Task.Run(async () => { await*/
            VerificaOrdemServico();/* }).Wait();*/
            //Task.Run(async () => { await CarregarNumeroOrcamento(); }).Wait();
            CarregarNumeroOrcamento();
            //Task.Run(async () => { appData.Combustiveis = await EnvioMethods.RetornaCombustiveis(); }).Wait();
        }

        private async Task CarregaCombustiveis()
        {
            
        }
        
        //protected override void OnDisappearing()
        //{
        //    base.OnDisappearing();
        //    this.Title = "Menu";
        //}


        private async void BotaoTeste_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new PaginaTeste());
            }
            catch (Exception error)
            {
                //await DisplayAlert("Erro", error.Message, "Ok");
            }
        }
    }
}
