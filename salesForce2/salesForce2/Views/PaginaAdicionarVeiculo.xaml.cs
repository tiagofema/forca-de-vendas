﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using salesForce2.Http;
using salesForce2.Methods;
using salesForce2.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace salesForce2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaAdicionarVeiculo : ContentPage
    {
        private List<CombustivelModels> ListaCombustiveis { get; set; }
        private List<CarroFipeModels> ListaCarroFipe { get; set; }
        private CarroFipeModels CarroFipe { get; set; }

        public PaginaAdicionarVeiculo()
        {
            InitializeComponent();
            var appData = (AppData)BindingContext;
            try
            {
                if (appData.Combustiveis[0] == null) throw new Exception();
                ListaCombustiveis = appData.Combustiveis;
                foreach (var c in ListaCombustiveis)
                {
                    CombustivelPicker.Items.Add(c.Tipo.ToUpper());
                }
                CombustivelPicker.IsEnabled = true;
            }
            catch
            {
                while (true)
                {
                    Task.Run(async () => { await GetCombustivel(); }).Wait();
                    if (ListaCombustiveis?[0] != null) break;
                }
            }
            //GetCombustivel();
            RetornaVeiculoMegaLaudo();
            //Task.Run(async () => { await RetornaVeiculoMegaLaudo(); }).Wait();
        }

        private async void GravarCliente_OnClicked_(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            appData.CurrentVeiculoCliente = new ClienteVeiculoModels();
            //----------------------------------------------------------------
            //CarroFipe = new CarroFipeModels { Id = 75 };
            //CorVeiculo.Text = "Azul";
            //Placa.Text = "AAA-2020";
            //Km.Text = "70000";
            //Ano.Text = "2017"; 
            //--------------------------------------------------------------
            ActivitySwitch(true);
            try
            {
                appData.CurrentVeiculoCliente.CarroFipeId = CarroFipe.Id;
            }
            catch
            {
                await DisplayAlert("Erro",
                    "Nenhum Veículo selecionado. Por favor busque um veículo e o selecione na lista e tente novamente", "Ok");
                return;
            }
            try
            {
                //appData.CurrentVeiculoCliente.Ano = Convert.ToInt32(VerificadoresDocumentos.LimpezaNumeros(Ano.Text));
                appData.CurrentVeiculoCliente.Ano = VerificadoresDocumentos.LimpezaNumeros(Ano.Text);
            }
            catch (Exception error)
            {
                await DisplayAlert("a", error.Message, "Ok");
                ActivitySwitch(false);
                return;
            }
            if (string.IsNullOrEmpty(CorVeiculo.Text))
            {
                await DisplayAlert("Erro", "Digite a cor do veículo.", "Ok");
                ActivitySwitch(false);
                return;
            }
            appData.CurrentVeiculoCliente.Cor = CorVeiculo.Text.ToUpper();
            if (string.IsNullOrEmpty(Placa.Text))
            {
                await DisplayAlert("Erro", "Digite uma placa válida para o veículo.", "Ok");
                ActivitySwitch(false);
                return;
            }
            if (Placa.Text.Length < 8)
            {
                await DisplayAlert("Erro", "Digite uma placa válida para o veículo.", "Ok");
                ActivitySwitch(false);
                return;
            }
            appData.CurrentVeiculoCliente.Placa = VerificadoresDocumentos.LimpezaPlaca(Placa.Text).ToUpper();
            try
            {
                appData.CurrentVeiculoCliente.Quilometragem = int.Parse(Km.Text);
            }
            catch
            {
                await DisplayAlert("Erro", "Digite somente número no campo Km.", "Ok");
                ActivitySwitch(false);
                return;
            }
            var combustivelIndex = CombustivelPicker.SelectedIndex;
            if (combustivelIndex < 0)
            {
                {
                    await DisplayAlert("Alerta", "Nenhum combustível selecionado.", "Ok");
                    ActivitySwitch(false);
                    CombustivelPicker.Focus();
                    return;
                }
            }
            appData.CurrentVeiculoCliente.CombustivelId = ListaCombustiveis[combustivelIndex].Id;
            appData.CurrentVeiculoCliente.Codigo = "0";
            if (!appData.AdicionarCliente)
            {
                appData.CurrentVeiculoCliente.ClienteId = appData.CurrentCliente.Id;
                var result = await EnvioMethods.EnvioVeiculo(appData.CurrentVeiculoCliente);
                Finalizar(result);
                ActivitySwitch(false);
            }
            else
            {

                var result = await EnvioMethods.EnvioCliente(appData.CurrentCliente, appData.CurrentVeiculoCliente);

                Finalizar(result);
                ActivitySwitch(false);
            }
        }

        private async void Finalizar(string result)
        {
            var appData = (AppData)BindingContext;
            if (result.ToUpper() == "TRUE")
            {
                if (appData.AdicionarCliente)
                    await DisplayAlert("Sucesso",
                        "O cliente foi gravado com sucesso", "Ok");
                else
                {
                    await DisplayAlert("Sucesso",
                        "O veículo foi gravado com sucesso", "Ok");
                }

                if (!appData.InsercaoClienteOs)
                {
                    appData.CurrentCliente = new ClienteModels();
                    appData.CurrentVeiculoCliente = new ClienteVeiculoModels();
                }
                NavegacaoMethods.NavegaMenuPrincipal();
                return;
            }
            //await DisplayAlert("Erro", result, "Ok");
            await DisplayAlert("Erro",
                    "Houve um ou mais problemas na inserção. Verifique se o cliente já está cadastrado.", "Ok");
        }

        private async void Combustivel_OnFocused(object sender, FocusEventArgs e)
        {
            CombustivelPicker.SelectedIndex = -1;
            CombustivelPicker.Focus();
            Combustivel.Unfocus();
            while (CombustivelPicker.SelectedIndex == -1)
            {
                await Task.Delay(25);
            }
            var combustivel = ListaCombustiveis[CombustivelPicker.SelectedIndex];
            Combustivel.Text = combustivel.Tipo;
            //CombustivelPicker.Title = combustivel.Tipo;
        }

        private async Task GetCombustivel()
        {
            try
            {
                var response = await Client.Http.GetAsync("home/RetornarTodosCombustiveis");
                var resultado = await response.Content.ReadAsStringAsync();
                ListaCombustiveis = JsonConvert.DeserializeObject<List<CombustivelModels>>(resultado);
            }
            catch
            {
                await DisplayAlert("Alerta", "Não foi possivel conectar-se ao servidor. Verifique sua conexão e tente novamente.", "Ok");
            }
            foreach (var c in ListaCombustiveis)
            {
                CombustivelPicker.Items.Add(c.Tipo.ToUpper());
            }
            //CombustivelPicker.IsEnabled = true;
        }

        private async Task GetVeiculos(object sender, EventArgs e)
        {
            ActivitySwitch(true);
            var appData = (AppData)BindingContext;
            if (string.IsNullOrEmpty(Veiculo.Text))
            {
                ActivitySwitch(false);
                return;
            }
            try
            {
                var response = await Client.Http.GetAsync("home/RetornarTodosCarrosFipe?descricao=" + Veiculo.Text + "&oficinaId=" + appData.CurrentOficina.Id);
                var resultado = await response.Content.ReadAsStringAsync();
                ListaCarroFipe = JsonConvert.DeserializeObject<List<CarroFipeModels>>(resultado);

                CarroPicker.Items.Clear();

                if (ListaCarroFipe.Count < 1)
                {
                    await DisplayAlert("Alerta",
                        "Nenhum veículo localizado. Verifique se o nome está correto e tente novamente.", "Ok");
                    CarroPicker.Unfocus();
                    ActivitySwitch(false);
                    return;
                }

                foreach (var f in ListaCarroFipe)
                {
                    CarroPicker.Items.Add(f.Descricao);
                }
                ActivitySwitch(false);
                EscolheVeiculo();
            }
            catch
            {
                await DisplayAlert("Erro",
                    "Nenhum veículo encontrado com essas especificações. Por favor verifique o campo e tente novamente.",
                    "Ok");
                ActivitySwitch(false);
            }
        }

        private async void EscolheVeiculo()
        {
            try
            {
                CarroPicker.SelectedIndex = -1;
                Veiculo.Unfocus();
                CarroPicker.Focus();
                while (CarroPicker.SelectedIndex == -1)
                {
                    await Task.Delay(25);
                }
                var carro = ListaCarroFipe[CarroPicker.SelectedIndex];
                Veiculo.Text = carro.Descricao.Length > 40 ? carro.Descricao.Substring(0, 40) : carro.Descricao;
                CarroFipe = carro;
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro", "Houve algum erro na seleção do veículo. Tente novamente.", "Ok");
            }
        }

        private void ActivitySwitch(bool option)
        {
            SalvandoActivity.IsRunning = option;
            SalvandoActivity.IsVisible = option;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
                //
            }
        }

        private async Task RetornaVeiculoMegaLaudo()
        {
            var appData = (AppData)BindingContext;
            if (!appData.InsercaoClienteOs) return;
            if (appData.CurrentVeiculoCliente == null) return;
            Placa.Text = appData.CurrentVeiculoCliente.Placa;
            CorVeiculo.Text = appData.CurrentVeiculoCliente.Cor;
            Ano.Text = appData.CurrentVeiculoCliente.Ano.ToString();
            var a = appData.CurrentVeiculoCliente.Descricao.Split(' ');
            try
            {
                if (a.Length > 2)
                {
                    Veiculo.Text = a[0] + " " + a[1];
                    await GetVeiculos(new object(), new EventArgs());
                }
                else if (a.Length > 0)
                {
                    Veiculo.Text = a[0];
                    await GetVeiculos(new object(), new EventArgs());
                }
            }
            catch (Exception erro)
            {
                //await DisplayAlert("Erro", erro.Message, "Ok");
            }
        }
    }
}