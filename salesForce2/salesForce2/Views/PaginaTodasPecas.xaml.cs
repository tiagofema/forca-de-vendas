﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using salesForce2.Http;
using salesForce2.Methods;
using salesForce2.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace salesForce2.Views
{
    [Preserve(AllMembers = true)]

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaTodasPecas : ContentPage
    {
        public ObservableCollection<ProdutoModels> Produtos { get; set; }
        private List<ProdutoModels> ListaProdutos { get; set; }
        public bool BuscaCompleta { get; set; }

        public PaginaTodasPecas()
        {
            InitializeComponent();
            //CarregarProdutos();
            TransformaPagina();
        }

        private void TransformaPagina()
        {
            var appData = (AppData)BindingContext;
            if (!appData.Aplicacoes) return;
            Busca.Placeholder = "VEÍCULO";
            Title = "PEÇAS APLICAÇÃO";
        }


        private void ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var items = this.Produtos;
            if (BuscaCompleta) return;
            if (PecasListView.IsRefreshing) return;
            if (items.Count < 10)
            {
                BuscaCompleta = true;
                return;
            }

            try
            {
                if (items != null && e.Item == items[items.Count - 30])
                {
                    CarregarMaisProdutos();
                }
            }
            catch
            {
                //ignore
            }
            PecasListView.ItemsSource = Produtos;
        }

        /*private void CarregarProdutos()
        {
            var appData = (AppData)BindingContext;
            var produtos = appData.CurrentProdutoList;
            ListarProdutos(produtos);
        }*/

        private void ListarProdutos(List<ProdutoModels> listaProdutos)
        {
            Produtos = new ObservableCollection<ProdutoModels>();
            foreach (var f in listaProdutos)
            {
                Produtos.Add(f);
            }
            if (Produtos.Count < 1)
            {
                DisplayAlert("Erro", "Não foi encontrado nenhum produto. Verifique a pesquisa e tente novamente.",
                    "Ok");
                //TipoDocumento.Source = appData.CurrentCliente.CnpjCpf.Length == 11 ? "CPF:" : "CNPJ:";
            }
            PecasListView.ItemsSource = Produtos;
            PecasListView.IsVisible = Produtos.Count > 0;
        }

        private void ProdutoSelecionadoListView(object sender, SelectedItemChangedEventArgs item)
        {
            var appData = (AppData)BindingContext;
            appData.CurrentProduto = (ProdutoModels)item.SelectedItem;
            if (item.SelectedItem == null) return;
            PecasListView.SelectedItem = null;

            IrPaginaPecas();
            //IrPaginaPecas((ProdutoModels)item.SelectedItem);
        }

        private async void IrPaginaPecas()
        {
            await Navigation.PushAsync(new PaginaPeca());
            //await Navigation.PushAsync(new PaginaPeca(produto));
        }

        private async void BuscaPecas(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            BuscaCompleta = false;
            appData.Busca = true;
            appData.ListaIndex = 0;
            try
            {
                appData.BuscaString = Regex.Replace(VerificadoresDocumentos.RemoverAcentos(Busca.Text),
                    "[^A-Za-z0-9 \\. -]", string.Empty);
            }
            catch
            {
                appData.BuscaString = "";
            }
            PecasListView.IsRefreshing = true;
            //Produtos = new ObservableCollection<ProdutoModels>();
            // PecasListView.ItemsSource = Produtos;
            //if (!appData.Busca) return;
            ListaProdutos = await BuscaFuncao(true);
            PecasListView.IsRefreshing = false;
            ListarProdutos(ListaProdutos);
        }

        private async void CarregarMaisProdutos()
        {
            var appData = (AppData)BindingContext;
            PecasListView.IsRefreshing = true;
            appData.ListaIndex += appData.QuantidadeProdutos;
            try
            {
                await BuscaFuncao(appData.Busca);
            }
            catch
            {
                await DisplayAlert("Alerta",
                    "Problema na busca completa. Verifique conexão com a internet e tente novamente.", "Ok");
                PecasListView.IsRefreshing = false;
                return;
            }
            if (ListaProdutos.Count < appData.QuantidadeProdutos) BuscaCompleta = true;

            foreach (var f in ListaProdutos)
            {
                Produtos.Add(f);
            }
            PecasListView.IsRefreshing = false;
        }

        private async Task<List<ProdutoModels>> BuscaFuncao(bool busca)
        {
            var appData = (AppData)BindingContext;
            //if (!busca)
            //{
            //    appData.BuscaString = "";
            //}
            PecasListView.IsVisible = true;
            try
            {
                //var response = await Client.Http.GetAsync("home/RetornarBuscaPecaIndex?id=" +
                //                                          appData.CurrentOficina.Id +
                //                                          "&descricao=" + appData.BuscaString + "&quantidade=" +
                //                                          appData.QuantidadeProdutos + "&index=" + appData.ListaIndex +
                //                                          "&aplicacao=" + appData.Aplicacoes);
                //var resultado = await response.Content.ReadAsStringAsync();
                //ListaProdutos = JsonConvert.DeserializeObject<List<ProdutoModels>>(resultado);
                ListaProdutos = await EnvioMethods.RetornaProdutoIndex(appData.CurrentOficina.Id, appData.BuscaString,
                    appData.QuantidadeProdutos, appData.ListaIndex, appData.Aplicacoes);

                return ListaProdutos;
            }
            catch (Exception error)
            {
                //await DisplayAlert("Erro", error.ToString(), "Ok");
                PecasListView.IsVisible = false;
                //await DisplayAlert("Erro",
                //    "Não foi possível encontrar nenhuma peça. Verifique a pesquisa e tente novamente.", "Ok");
                return new List<ProdutoModels>();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {

            }
        }

        private async void IrPaginaCodigoBarras(object o,EventArgs e)
        {
            var scanPage = new ZXingScannerPage();

            scanPage.OnScanResult += (result) =>
            {
                // Stop scanning
                scanPage.IsScanning = false;
                // Pop the page and show the result
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PopAsync();
                    //DisplayAlert("Scanned Barcode", result.Text, "OK");
                    Busca.Text = result.Text;
                    BuscaPecas(new object(), new EventArgs());
                });
            };
                await Navigation.PushAsync(scanPage);
        }
    }
}