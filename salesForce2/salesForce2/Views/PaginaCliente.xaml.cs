﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;
using salesForce2.Http;
using salesForce2.Methods;
using salesForce2.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace salesForce2.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaCliente : ContentPage
    {
        public ObservableCollection<ClienteVeiculoModels> ListaVeiculos { get; set; }
        //private List<ClienteVeiculoModels> ListaVeiculos { get; set; }

        public PaginaCliente()
        //public PaginaCliente(ClienteModels cliente)
        {
            InitializeComponent();
            Title = "";
            //BindingContext = cliente;
            Titulador();
            BuscaVeiculos();
            VeiculosListView.IsRefreshing = false;
        }

        private void Titulador()
        {
            var appData = (AppData)BindingContext;
            if (string.IsNullOrEmpty(appData.BuscaString))
            {
                appData.BuscaString = appData.CurrentCliente.NomeFantasia;
            }

            if (string.IsNullOrEmpty(appData.CurrentCliente.NomeFantasia)) return;
            var split = appData.CurrentCliente.NomeFantasia.Split(' ');
            var i = 0;
            var contador = split.Count();
            if (contador >= 5)
            {
                while (i < 5 && Title.Length < 18)
                {
                    Title = Title + " " + split[i];
                    i++;
                }
            }
            else
            {
                while (i < contador && Title.Length < 15)
                {
                    if (i > 2 && split[i].Length < 3)
                    {
                        i++;
                    }
                    else
                    {
                        Title = Title + " " + split[i];
                        i++;
                    }
                }
            }
            try
            {
                if (appData.CurrentCliente.Cep.Length == 8)
                    appData.CurrentCliente.Cep = appData.CurrentCliente.Cep.Substring(0, 5) + "-" +
                                                 appData.CurrentCliente.Cep.Substring(5, 7);
            }
            catch
            {
                
            }
        }

        private async void BuscaVeiculos()
        {
            var appData = (AppData)BindingContext;
            ListaVeiculos = new ObservableCollection<ClienteVeiculoModels>();
            try
            {
                VeiculosListView.IsRefreshing = true;
                //var response =
                //    await Client.Http.GetAsync("home/RetornarClienteVeiculos?documento=" +appData.CurrentCliente.Id);
                //var resultado = await response.Content.ReadAsStringAsync();
                //var lista = JsonConvert.DeserializeObject<List<ClienteVeiculoModels>>(resultado);
                var lista = await EnvioMethods.RetornarClienteVeiculos(appData.CurrentCliente.Id);
                VeiculosListView.IsRefreshing = false;
                foreach (var f in lista)
                {
                    ListaVeiculos.Add(f);
                }
                VeiculosListView.ItemsSource = ListaVeiculos;
                VeiculosListView.HeightRequest = (VeiculosListView.RowHeight + 35) * ListaVeiculos.Count;
                //VeiculosListView.HeightRequest = ListaVeiculos.Count * 110;
            }
            catch (Exception error)
            {
                VeiculosListView.IsRefreshing = false;
                await DisplayAlert("Erro","Houve um erro na busca dos veículos.", "Ok");
            }
            VeiculosListView.IsRefreshing = false;
        }

        private async void AdicionarVeículo_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            appData.AdicionarCliente = false;
            await Navigation.PushAsync(new PaginaAdicionarVeiculo());
        }

        private void VeiculoSelecionadoListView (object sender, SelectedItemChangedEventArgs item)
        {
            if (item.SelectedItem == null) return;
            VeiculosListView.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {

            }
        }
    }
}