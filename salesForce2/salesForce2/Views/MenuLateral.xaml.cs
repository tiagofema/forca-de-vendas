﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using salesForce2.Http;
using salesForce2.Methods;
using salesForce2.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace salesForce2.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuLateral : ContentPage
    {
        public MenuLateral()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        /*protected override bool OnBackButtonPressed()
        {
            if (Device.OS == TargetPlatform.Android)
                DependencyService.Get<IAndroidMethods>().CloseApp();

            return base.OnBackButtonPressed();
        }*/


        private void DeslogarBotao(object sender, EventArgs e)
        {
           
            Deslogar();
        }

        private async void Deslogar()
        {
            var resposta = await DisplayAlert("Logout", "Deseja realmente sair?", "Sim", "Não");
            if (!resposta) return;
            var appData = (AppData)BindingContext;
            //var values = new Dictionary<string, string>
            //{
            //    { "funcionarioId", appData.CurrentFuncionario.Id.ToString() }
            //};

            //var content = new FormUrlEncodedContent(values);
            //var response = await Client.Http.PostAsync("home/DeslogaFuncionario", content);
            //var resultado = await response.Content.ReadAsStringAsync();
            var resultado = await EnvioMethods.DeslogaFuncionario(appData.CurrentFuncionario.Id);
            switch (resultado)
            {
                case "TRUE": break;
                case "FALSE":
                    await DisplayAlert("Erro",
                        "Não foi possivel conectar-se ao servidor para concluir a operação de logoff. Verifique sua conexão com a internet e tente novamente.",
                        "Ok");
                    return;
                default:
                    await DisplayAlert("Erro",
                        "Não foi possivel conectar-se ao servidor para concluir a operação de logoff. Verifique sua conexão com a internet e tente novamente.",
                        "Ok");
                    return;
            }
            appData.CurrentOficina = new OficinaModels();
            appData.CurrentFuncionario = new FuncionarioModels();
            Application.Current.Properties["appData"] = JsonConvert.SerializeObject(appData);

            Application.Current.MainPage = new NavigationPage(new TelaLoginEmpresa());
        }

        private async void BotaoConsultaPecas_OnClicked(object sender, EventArgs e)
        {
            try
            {
                App.MasterDetail.IsPresented = false;
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaTodasPecas());
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }


        private async void BotaoConsultaDeClientes_OnClicked(object sender, EventArgs e)
        {
            try
            {
                App.MasterDetail.IsPresented = false;
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaTodosClientes());
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }

        private async void BotaoAberturaDeOs_OnClicked(object sender, EventArgs e)
        {
            try
            {
                App.MasterDetail.IsPresented = false;
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaOrdemServico());
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }

        private async void BotaoCadastrarCliente_OnClicked(object sender, EventArgs e)
        {
            try
            {
                App.MasterDetail.IsPresented = false;
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaAdicionarCliente());
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }
    }
}