﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using salesForce2.Http;
using salesForce2.Methods;
using salesForce2.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace salesForce2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaOrdemServico : ContentPage
    {
        private ClienteModels Clientes { get; set; }
        private List<ClienteVeiculoModels> CarrosCliente { get; set; }
        private List<CombustivelModels> ListaCombustiveis { get; set; }
        private List<CarroFipeModels> ListaCarroFipe { get; set; }
        private CarroFipeModels CarroFipe { get; set; }
        private CombustivelModels Combustivel { get; set; }
        private bool CarregandoCarroListView { get; set; }
        private bool CarregandoCliente { get; set; }
        private bool Gravando { get; set; }

        public PaginaOrdemServico()
        {
            InitializeComponent();
            var appData = (AppData)BindingContext;
            try
            {
                if (appData.Combustiveis[0] == null) throw new Exception();
                ListaCombustiveis = appData.Combustiveis;
                foreach (var c in ListaCombustiveis)
                {
                    //var espaco = "";
                    //var a = (50 -c.Tipo.Length)/2;
                    //for (var b=0; b<= a; b++)
                    //{
                    //    espaco = espaco + " ";
                    //}
                    //var tipo = espaco + c.Tipo.ToUpper();
                    //CombustivelPicker.Items.Add(tipo);
                    CombustivelPicker.Items.Add(c.Tipo.ToUpper());
                }
                CombustivelPicker.IsEnabled = true;
            }
            catch
            {
                while (true)
                {
                    Task.Run(async () => { await GetCombustivel(); }).Wait();
                    if (ListaCombustiveis?[0] != null) break;
                }
            }
            //Task.Run(async () => { await GetCombustivel(); }).Wait();
            //GetCombustivel();
            appData.NumeroDocumento = "Nro.: Documento:";
            GetClientes();
            //Task.Run(async () => { await GetClientes(); }).Wait();
            //GetClientes();
        }

        //private async Task Inicializa()
        //{
        //    Veiculo_OnFocused(new object(), new FocusEventArgs(Veiculo, true));
        //}

        private async void Gravar(object sender, EventArgs e)
        {
            if (Gravando) return;

            CarregandoGravacaoOnOff(true);
            //var ordemServico = new OrdemServicoModels();
            var appData = (AppData)BindingContext;
            appData.CurrentOrdemServico.NumeroOrcamento = appData.NumeroOrcamento;
            appData.CurrentOrdemServico.OficinaId = appData.CurrentOficina.Id;

            try
            {
                var a = Nome.Text;
                var b = NumeroDocumento.Text;
                appData.CurrentOrdemServico.ClienteId = appData.CurrentCliente.Id;
            }
            catch
            {
                await DisplayAlert("Erro",
                    "Nenhum cliente selecionado. Por favor digite um CPF/CNPJ e tente novamente.", "Ok");
                CarregandoGravacaoOnOff(false);
                return;
            }

            if (appData.CurrentCliente.Id == 0 && !string.IsNullOrEmpty(NumeroDocumento.Text))
            {
                try
                {
                    var documento = Regex.Replace(NumeroDocumento.Text, "[^0-9]", string.Empty);
                    //var resposta = await Client.Http.GetAsync("home/RetornarClienteCpfCnpj?id=" +
                    //                                          appData.CurrentOficina.Id +
                    //                                          "&cpfCnpj=" + documento);
                    //var resultado = await resposta.Content.ReadAsStringAsync();
                    //Clientes = JsonConvert.DeserializeObject<ClienteModels>(resultado);
                    Clientes = await EnvioMethods.RetornarClienteCpfCnpj(appData.CurrentOficina.Id, documento);
                    Nome.Text = Clientes.NomeFantasia;
                    appData.CurrentCliente = Clientes;
                    appData.CurrentOrdemServico.ClienteId = appData.CurrentCliente.Id;
                }
                catch
                {
                    await DisplayAlert("Erro", "Nenhum cliente selecionado. Por favor selecione um para continuar.", "Ok");
                    CarregandoGravacaoOnOff(false);
                    return;
                }
            }
            try
            {
                appData.CurrentOrdemServico.CarroFipeId = CarroFipe.Id;
            }
            catch
            {
                await DisplayAlert("Erro",
                    "Nenhum Veículo selecionado. Por favor busque um veículo e o selecione na lista e tente novamente", "Ok");
                CarregandoGravacaoOnOff(false);

                return;
            }

            if (int.TryParse(AnoEntry.Text, out var ano))
            {
                appData.CurrentOrdemServico.Ano = ano;
            }
            else
            {
                await DisplayAlert("Erro", "O ano digitado é inválido. Digite no formato AAAA.", "Ok");
                AnoEntry.Focus();
                CarregandoGravacaoOnOff(false);
                return;
            }
            if (decimal.TryParse(Km.Text, out var quilometragem))
            {
                appData.CurrentOrdemServico.Quilometragem = quilometragem;
            }
            else
            {
                await DisplayAlert("Erro", "A quilometragem digitada é inválida. Digite no formato xxxx,xxx ou somente XXXXX .", "Ok");
                Km.Focus();
                CarregandoGravacaoOnOff(false);
                return;
            }
            appData.CurrentOrdemServico.FuncionarioId = appData.CurrentFuncionario.Id;
            var combustivelIndex = CombustivelPicker.SelectedIndex;
            try
            {
                if (combustivelIndex < 0 || Combustivel.Id == -1)
                {
                    {
                        await DisplayAlert("Alerta", "Nenhum combustível selecionado.", "Ok");
                        CombustivelPicker.Focus();
                        CarregandoGravacaoOnOff(false);
                        return;
                    }
                }
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro", "Houve algum problema carregando o combustível. Tente novamente.", "Ok");
                CarregandoGravacaoOnOff(false);
                return;
            }
            //appData.CurrentOrdemServico.CombustivelId = ListaCombustiveis[combustivelIndex].Id;
            appData.CurrentOrdemServico.CombustivelId = Combustivel.Id;
            appData.CurrentOrdemServico.Avarias =
                string.IsNullOrEmpty(appData.Avarias) ? "" : VerificadoresDocumentos.LimpezaGeral(VerificadoresDocumentos.RemoverAcentos(appData.Avarias.ToUpper()));
            appData.CurrentOrdemServico.Defeitos =
                string.IsNullOrEmpty(appData.Defeitos) ? "" : VerificadoresDocumentos.LimpezaGeral(VerificadoresDocumentos.RemoverAcentos(appData.Defeitos.ToUpper()));
            appData.CurrentOrdemServico.Observacao =
                string.IsNullOrEmpty(appData.Observacao) ? "" : VerificadoresDocumentos.LimpezaGeral(VerificadoresDocumentos.RemoverAcentos(appData.Observacao.ToUpper()));
            try
            {
                if (PlacaEntry.Text.Length < 8)
                {
                    await DisplayAlert("Erro",
                        "Quantidade de digitos inválida no campo placa. O formato é AAA-0000. Tente novamente.", "Ok");
                    return;
                }
            }
            catch
            {
                await DisplayAlert("Erro", "Por favor digite uma placa válida.", "Ok");
                return;
            }
            appData.CurrentOrdemServico.Placa = VerificadoresDocumentos.LimpezaPlaca(PlacaEntry.Text.ToUpper());
            appData.CurrentOrdemServico.DataEmissao = DateTime.Now;
            appData.CurrentOrdemServico.WorkMotorId = 0;
            appData.CurrentOrdemServico.Cor = string.IsNullOrEmpty(CorEntry.Text) ? "NÃO DECLARADA" : VerificadoresDocumentos.LimpezaGeral(CorEntry.Text.ToUpper());

            try
            {
                if (CarrosCliente.All(c => VerificadoresDocumentos.LimpezaPlaca(c.Placa) != VerificadoresDocumentos.LimpezaPlaca(appData.CurrentOrdemServico.Placa)))
                {
                    var carroCliente = new ClienteVeiculoModels
                    {
                        //Ano = int.Parse(AnoEntry.Text),
                        Ano = AnoEntry.Text,
                        Placa = VerificadoresDocumentos.LimpezaPlaca(PlacaEntry.Text.ToUpper()),
                        CarroFipeId = appData.CurrentOrdemServico.CarroFipeId,
                        ClienteId = appData.CurrentCliente.Id,
                        CombustivelId = Combustivel.Id,
                        CombustivelTipo = Combustivel.Tipo,
                        Cor = CorEntry.Text.ToUpper(),
                        Descricao = CarroFipe.Descricao,
                        Quilometragem = decimal.Parse(Km.Text)
                    };
                    try
                    {
                        var resultBool = await InsereVeiculo(carroCliente);
                        if (!resultBool) return;
                    }
                    catch
                    {
                        CarregandoGravacaoOnOff(false);
                        return;
                    }
                }
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro", "Houve um erro ao tentar inserir o veículo. Verifique os dados e tente novamente.", "Ok");
                CarregandoGravacaoOnOff(false);
                return;
            }
            //var ordemSerializada = JsonConvert.SerializeObject(appData.CurrentOrdemServico);
            //var response = await Client.Http.GetAsync("home/RecebeOrdemServico?ordem=" + ordemSerializada);
            //var result = await response.Content.ReadAsStringAsync();
            var result = await EnvioMethods.RecebeOrdemServico(appData.CurrentOrdemServico);

            if (result.ToUpper() == "TRUE")
            {
                await DisplayAlert("Sucesso",
                    "A ordem de serviço foi enviada para o WorkMotor.", "Ok");
                appData.NumeroOrcamento++;
                appData.Avarias = "";
                appData.Defeitos = "";
                appData.Observacao = "";
                CarregandoGravacaoOnOff(false);


                appData.CurrentCliente = new ClienteModels();
                appData.CurrentClienteVeiculo = new List<ClienteVeiculoModels>();
                NavegacaoMethods.NavegaMenuPrincipal();
                return;
            }
            //await DisplayAlert("Erro", result, "Ok");
            await DisplayAlert("Erro",
                "Houve um ou mais problemas na inserção. Verifique os dados e tente novamente.", "Ok");

            CarregandoGravacaoOnOff(false);
        }

        private async void NumeroDocumento_OnCompleted(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            if (CarregandoCliente) return;
            CarregandoCliente = true;
            foreach (var c in CarroPicker.Items.ToList())
            {
                CarroPicker.Items.Remove(c);
            }
            CarregandoGravacaoOnOff(true);
            string documento;
            try
            {
                documento = Regex.Replace(NumeroDocumento.Text, "[^0-9]", string.Empty);
            }
            catch
            {
                await DisplayAlert("Erro", "Nenhum CPF/CNPJ a ser pesquisado. Preencha o campo e tente novamente.",
                    "Ok");
                CarregandoCliente = false;
                CarregandoGravacaoOnOff(false);
                return;
            }
            try
            {
                //var response = await Client.Http.GetAsync("home/RetornarClienteCpfCnpj?id=" +
                //                                          appData.CurrentOficina.Id +
                //                                          "&cpfCnpj=" + documento);
                //var resultado = await response.Content.ReadAsStringAsync();
                //Clientes = JsonConvert.DeserializeObject<ClienteModels>(resultado);
                Clientes = await EnvioMethods.RetornarClienteCpfCnpj(appData.CurrentOficina.Id,documento);
                Nome.Text = Clientes.NomeFantasia;
                appData.CurrentCliente = Clientes;
            }
            catch
            {
                var resposta = await DisplayAlert(
                    "Erro", "Não foi encontrado o cliente com esse CPF/CNPJ.", "Adicionar Cliente", "Voltar");
                Nome.Text = " NOME DO CLIENTE ";
                CarregandoCliente = false;
                CarregandoGravacaoOnOff(false);
                if (!resposta) return;
                appData.CurrentCliente = new ClienteModels
                {
                    CnpjCpf = documento
                };
                appData.InsercaoClienteOs = true;
                CarregandoGravacaoOnOff(false);
                CarregandoCliente = false;
                Navigation.InsertPageBefore(new PaginaAdicionarCliente(), this);
                await Navigation.PopAsync().ConfigureAwait(false);
            }
            await CarregaVeiculos();
            CarregandoCliente = false;
            CarregandoGravacaoOnOff(false);
            //ActivityOff();
            //}
        }

        private void BuscaDocumento_OnClicked(object sender, EventArgs e)
        {
            NumeroDocumento_OnCompleted(new object(), new EventArgs());
        }

        private async void GetVeiculos(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            CarregandoGravacaoOnOff(true);
            //ActivityCarro.IsRunning = true;
            if (string.IsNullOrEmpty(Veiculo.Text))
            {
                await DisplayAlert("Alerta", "Nenhum veículo a ser pesquisdo. Preencha o campo e tente novamente",
                    "Ok");
                CarregandoGravacaoOnOff(false);
                return;
            }
            try
            {
                //var response = await Client.Http.GetAsync("home/RetornarTodosCarrosFipe?descricao=" + Veiculo.Text + "&oficinaId=" + appData.CurrentOficina.Id);
                //var resultado = await response.Content.ReadAsStringAsync();
                //ListaCarroFipe = JsonConvert.DeserializeObject<List<CarroFipeModels>>(resultado);
                ListaCarroFipe = await EnvioMethods.RetornarTodosCarrosFipe(Veiculo.Text,appData.CurrentOficina.Id);
                CarregandoCarroListView = false;

                CarroPicker.Items.Clear();

                if (ListaCarroFipe.Count < 1)
                {
                    await DisplayAlert("Alerta",
                        "Nenhum veículo localizado. Verifique se o nome está correto e tente novamente.", "Ok");
                    CarroPicker.Unfocus();
                    return;
                }

                foreach (var f in ListaCarroFipe)
                {
                    CarroPicker.Items.Add(f.Descricao);
                }
                //FuncionarioPicker.IsEnabled = true;
                //CarroPicker.ItemsSource = ListaCarroFipe;
                CarregandoGravacaoOnOff(false);
                await EscolheVeiculo();
            }
            catch (Exception error)
            {
                CarregandoGravacaoOnOff(false);

                //await DisplayAlert("Erro", error.ToString(), "Ok");
                await DisplayAlert("Erro",
                    "Nenhum veículo encontrado com essas especificações. Por favor verifique o campo e tente novamente.",
                    "Ok");
            }

            AnoEntry.Text = "";
            Km.Text = "";
            CombustivelPicker.SelectedIndex = -1;
            PlacaEntry.Text = "";
            CombustivelEntry.Text = "";
            CorEntry.Text = "";
            //ActivityCarro.IsRunning = false;
            CarregandoGravacaoOnOff(false);

        }

        private async Task EscolheVeiculo()
        {
            var appData = (AppData)BindingContext;
            CarroPicker.SelectedIndex = -1;
            CarroPicker.Focus();
            Veiculo.Unfocus();
            while (CarroPicker.SelectedIndex == -1)
            {
                await Task.Delay(25);
            }
            var carro = ListaCarroFipe[CarroPicker.SelectedIndex];
            Veiculo.Text = carro.Descricao;
            CarroFipe = carro;
            appData.CurrentOrdemServico.CarroFipeId = carro.Id;

        }

        private void Ano_OnCompleted(object sender, EventArgs e)
        {
            Km.Focus();
        }

        private void Km_OnCompleted(object sender, EventArgs e)
        {
            CombustivelPicker.Focus();
        }

        private async Task GetClientes()
        {
            var appData = (AppData)BindingContext;
            appData.InsercaoClienteOs = false;
            CarregandoGravacaoOnOff(false);
            try
            {
                Nome.Text = appData.CurrentCliente.NomeFantasia;
                NumeroDocumento.Text = appData.CurrentCliente.CnpjCpf;
                appData.CurrentOrdemServico = new OrdemServicoModels();
                await CarregaVeiculos();
                if (!string.IsNullOrEmpty(appData.CurrentCliente.NomeFantasia)) return;
                Nome.Text = "NOME DO CLIENTE";
                NumeroDocumento.Placeholder = "      CPF/CNPJ         ";
                appData.CurrentOrdemServico.ClienteId = appData.CurrentCliente.Id;
            }
            catch
            {
                //   
            }
        }

        private async void BotaoDetalhes_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaginaObservacoes());
        }

        private async void CombustivelEntry_OnFocused(object sender, FocusEventArgs e)
        {
            CombustivelPicker.SelectedIndex = -1;
            CombustivelPicker.Focus();
            CombustivelEntry.Unfocus();
            while (CombustivelPicker.SelectedIndex == -1)
            {
                await Task.Delay(25);
            }
            Combustivel = ListaCombustiveis[CombustivelPicker.SelectedIndex];
            CombustivelEntry.Text = Combustivel.Tipo.ToUpper();
        }

        private async Task GetCombustivel()
        {
            try
            {
                var response = await Client.Http.GetAsync("home/RetornarTodosCombustiveis");
                var resultado = await response.Content.ReadAsStringAsync();
                ListaCombustiveis = JsonConvert.DeserializeObject<List<CombustivelModels>>(resultado);
            }
            catch
            {
                await DisplayAlert("Alerta", "Não foi possivel conectar-se ao servidor. Verifique sua conexão com a internet e tente novamente.", "Ok");
            }
            foreach (var c in ListaCombustiveis)
            {
                CombustivelPicker.Items.Add(c.Tipo.ToUpper());
            }
            CombustivelPicker.IsEnabled = true;
        }

        private void CarregandoGravacaoOnOff(bool ativo)
        {
            Gravando = ativo;
            CarregandoGravacao.IsRunning = ativo;
            CarregandoGravacao.IsVisible = ativo;
            //CarregandoGravacao.IsEnabled = ativo;
        }

        private async Task CarregaVeiculos()
        {
            CarregandoGravacaoOnOff(true);
            var appData = (AppData)BindingContext;
            try
            {
                if (appData.CurrentCliente.Id == 0)
                {
                    CarregandoGravacaoOnOff(false);
                    return;
                }
                CarrosCliente = new List<ClienteVeiculoModels>();
                var values2 = new Dictionary<string, string>
                {
                    {"documento",appData.CurrentCliente.Id.ToString() }
                };

                var content2 = new FormUrlEncodedContent(values2);
                var response = await Client.Http.PostAsync("home/RetornarClienteVeiculos", content2);
                //var response = await Client.Http.GetAsync("home/RetornarClienteVeiculos?documento=" + appData.CurrentCliente.Id);
                var resultado = await response.Content.ReadAsStringAsync();
                CarrosCliente = JsonConvert.DeserializeObject<List<ClienteVeiculoModels>>(resultado);

                try
                {
                    ////[GET]
                    //ListaCarroFipe = new List<CarroFipeModels>();
                    //var response2 = await Client.Http.GetAsync(
                    //    "home/RetornaCarrosFipeCliente?carrosCliente=" + JsonConvert.SerializeObject(CarrosCliente));
                    //var resultado2 = await response2.Content.ReadAsStringAsync();
                    //ListaCarroFipe = JsonConvert.DeserializeObject<List<CarroFipeModels>>(resultado2);
                    //[POST]

                    var values = new Dictionary<string, string>
                    {
                        { "carrosCliente", JsonConvert.SerializeObject(CarrosCliente) }
                    };

                    var content = new FormUrlEncodedContent(values);
                    var response2 = await Client.Http.PostAsync("home/RetornaCarrosFipeCliente", content);
                    var responseString = await response2.Content.ReadAsStringAsync();
                    ListaCarroFipe = JsonConvert.DeserializeObject<List<CarroFipeModels>>(responseString);

                    if (CarrosCliente == null || CarrosCliente.Count == 0)
                    {
                        CarregandoGravacaoOnOff(false);
                        return;
                    }
                    //var carroCliente = new ClienteVeiculoModels();
                    if (CarrosCliente.Count == 1)
                    {
                        appData.CurrentOrdemServico.CarroFipeId = CarrosCliente[0].CarroFipeId;
                        CarroFipe = ListaCarroFipe.First(c1 => c1.Id == CarrosCliente[0].CarroFipeId);
                    }
                    else if (CarrosCliente.Count > 1)
                    {
                        ListaCarroFipe = new List<CarroFipeModels>(ListaCarroFipe.OrderBy(c => c.Descricao));
                        foreach (var f in ListaCarroFipe)
                        {
                            CarroPicker.Items.Add(f.Descricao);
                        }
                        await EscolheVeiculo();
                    }

                    CarregandoGravacaoOnOff(false);
                    var carroCliente = CarrosCliente.First(c1 => c1.CarroFipeId == appData.CurrentOrdemServico.CarroFipeId);
                    Veiculo.Text = carroCliente.Descricao.Length > 30 ? carroCliente.Descricao.Substring(0, 30) : carroCliente.Descricao;
                    AnoEntry.Text = carroCliente.Ano.ToString();
                    Km.Text = carroCliente.Quilometragem.ToString();
                    CombustivelEntry.Text = carroCliente.CombustivelTipo;
                    CorEntry.Text = carroCliente.Cor;
                    Combustivel = new CombustivelModels
                    {
                        Id = carroCliente.CombustivelId,
                        Tipo = carroCliente.CombustivelTipo
                    };
                    appData.CurrentOrdemServico.CombustivelId = carroCliente.CombustivelId;
                    PlacaEntry.Text = carroCliente.Placa;
                    CombustivelPicker.SelectedIndex = 1;
                }
                catch (Exception e)
                {
                    //await DisplayAlert("Erro", e.Message, "Ok");
                    await DisplayAlert("Erro", "Houve um erro buscando os veículos. Verifique sua conexão com a internet e tente novamente.", "Ok");
                    CarregandoGravacaoOnOff(false);
                    //
                }
            }
            catch (Exception e)
            {
                await DisplayAlert("Erro", "Houve um erro buscando os veículos. Verifique sua conexão com a internet e tente novamente. ", "Ok");
                CarregandoGravacaoOnOff(false);
                //
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
                //
            }
        }

        private async void Veiculo_OnFocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (Veiculo.Text.Length > 0) return;
            }
            catch
            {
                return;
            }
            try
            {
                if (Veiculo.Text.Length < 2) return;
            }
            catch
            {
                return;
            }

            try
            {
                ListaCarroFipe = new List<CarroFipeModels>();
                if (CarrosCliente.Count > 0)
                {
                    await EscolheVeiculo();
                }
            }
            catch
            {
                //
            }

        }

        private async Task<bool> InsereVeiculo(ClienteVeiculoModels carroCliente)
        {
            try
            {
                var veiculoSerializado1 = JsonConvert.SerializeObject(carroCliente);
                //var response1 =
                //    await Client.Http.GetAsync("home/AdicionarVeiculoCliente?&veiculo=" + veiculoSerializado1);
                //var result2 = await response1.Content.ReadAsStringAsync();
                var result2 = await EnvioMethods.AdicionarVeiculoCliente(veiculoSerializado1);

                if (result2.ToUpper() == "TRUE") return true;
                await DisplayAlert("Erro", "Não foi possivel fazer a inclusão do veículo. Verifique se os dados estão corretos e tente novamente.", "Ok");
                return false;
            }
            catch (Exception error)
            {
                await DisplayAlert("Erro",
                    "Houve algum problema na inserção. Verifique os dados e sua conexão com a internet e tente novamente.",
                    "Ok");
                return false;
            }
        }

        private async void BuscaPlaca_OnClicked(object sender, EventArgs e)
        {

            if (Gravando) return;
            CarregandoGravacaoOnOff(true);
            var appData = (AppData)BindingContext;
            try
            {
                if (PlacaBusca.Text.Length < 8)
                {
                    await DisplayAlert("Aviso", "A placa é inválida. Verifique os digitos e tente novamente.", "Ok");
                    CarregandoGravacaoOnOff(false);
                    return;
                }
                try
                {
                    var placa = VerificadoresDocumentos.LimpezaGeral(PlacaBusca.Text);
                    //var response1 =
                    //    await Client.Http.GetAsync("home/RetornaCarroCliente?&placa=" + placa + "&oficinaId=" + appData.CurrentOficina.Id);
                    //var result = await response1.Content.ReadAsStringAsync();
                    //var clienteComVeiculo = JsonConvert.DeserializeObject<ClienteComVeiculoModels>(result);
                    var clienteComVeiculo = await EnvioMethods.RetornaCarroCliente(placa, appData.CurrentOficina.Id);

                    appData.CurrentCliente = clienteComVeiculo.Cliente;
                    appData.CurrentVeiculoCliente = clienteComVeiculo.Veiculo;
                    appData.CurrentVeiculoCliente.Placa = VerificadoresDocumentos.LimpezaPlaca(appData.CurrentVeiculoCliente.Placa);

                    NumeroDocumento.Text = appData.CurrentCliente.CnpjCpf;
                    Nome.Text = appData.CurrentCliente.NomeFantasia;

                    Veiculo.Text = appData.CurrentVeiculoCliente.Descricao.Length > 30 ? appData.CurrentVeiculoCliente.Descricao.Substring(0, 30) : appData.CurrentVeiculoCliente.Descricao;
                    AnoEntry.Text = appData.CurrentVeiculoCliente.Ano.ToString();
                    Km.Text = appData.CurrentVeiculoCliente.Quilometragem.ToString();
                    CombustivelEntry.Text = appData.CurrentVeiculoCliente.CombustivelTipo;
                    CorEntry.Text = appData.CurrentVeiculoCliente.Cor.ToUpper();
                    Combustivel = new CombustivelModels
                    {
                        Id = appData.CurrentVeiculoCliente.CombustivelId,
                        Tipo = appData.CurrentVeiculoCliente.CombustivelTipo
                    };
                    appData.CurrentOrdemServico.CombustivelId = appData.CurrentVeiculoCliente.CombustivelId;
                    PlacaEntry.Text = appData.CurrentVeiculoCliente.Placa;
                    CombustivelPicker.SelectedIndex = 1;

                    CarroFipe = new CarroFipeModels
                    {
                        Descricao = appData.CurrentVeiculoCliente.Descricao,
                        Id = appData.CurrentVeiculoCliente.CarroFipeId,
                        OficinaId = appData.CurrentOficina.Id,
                        CodigoFipe = "0",
                        AnoFinal = "1900",
                        AnoInicial = "2000"
                    };
                    CarrosCliente = new List<ClienteVeiculoModels>();
                    CarrosCliente.Add(appData.CurrentVeiculoCliente);
                    CarregandoGravacaoOnOff(false);
                    return;
                }
                catch (Exception error)
                {
                    CarregandoGravacaoOnOff(false);
                    var resposta = await DisplayAlert("Erro",
                        "Placa não encontrada. Verifique os digitos e tente novamente.",
                        "Adicionar Cliente", "Voltar");
                    if (!resposta) return;
                    appData.InsercaoClienteOs = true;
                    appData.CurrentVeiculoCliente = new ClienteVeiculoModels
                    {
                        Placa = VerificadoresDocumentos.LimpezaPlaca(PlacaBusca.Text)
                    };
                    Navigation.InsertPageBefore(new PaginaAdicionarCliente(), this);
                    CarregandoGravacaoOnOff(false);
                    await Navigation.PopAsync().ConfigureAwait(false);
                }
            }
            catch
            {
                CarregandoGravacaoOnOff(false);
                await DisplayAlert("Erro", "Por favor digite uma placa e tente novamente.", "Ok");
                return;
            }
        }

        private void PlacaBusca_OnCompleted(object sender, EventArgs e)
        {
            BuscaPlaca_OnClicked(new object(), new EventArgs());
        }
    }
}