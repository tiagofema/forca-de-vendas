﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace salesForce2.Headers
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrincipalHeader : ContentPage
    {
        public PrincipalHeader()
        {
            InitializeComponent();
        }
    }
}