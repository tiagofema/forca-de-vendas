﻿using Android.App;
using Android.OS;

namespace salesForce2.Droid
{
    [Activity(Theme = "@style/Theme.Splash",
        Icon = "@drawable/workmotorapplaucher",
        MainLauncher = true,
        NoHistory = false)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            StartActivity(typeof(MainActivity));
            //Task.Delay(3000);
            this.Finish();
            // Create your application here
        }
    }
}