﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;

namespace salesForce2.Droid
{
    [Activity(Label = "WorkMotor", Icon = "@drawable/workmotorapplaucher", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.AdjustResize)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity //FormsAppCompatActivity   FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            //--------------------------------------------------------
            //var uiOptions = (int)Window.DecorView.SystemUiVisibility;

            //uiOptions |= (int)SystemUiFlags.LowProfile;
            //uiOptions |= (int)SystemUiFlags.Fullscreen;
            //uiOptions |= (int)SystemUiFlags.HideNavigation;
            //uiOptions |= (int)SystemUiFlags.ImmersiveSticky;

            //Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
            //------------------------------------------------------------------
            //ADICIONANDO O ZXING PARA LER CÓDIGO DE BARRAS
            ZXing.Net.Mobile.Forms.Android.Platform.Init();
            // --------------
            LoadApplication(new App());

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            global::ZXing.Net.Mobile.Forms.Android.PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        //protected override void OnStart()
        //{
        //    base.OnStart();
        //    var uiOptions = (int)Window.DecorView.SystemUiVisibility;

        //    uiOptions |= (int)SystemUiFlags.LowProfile;
        //    uiOptions |= (int)SystemUiFlags.Fullscreen;
        //    uiOptions |= (int)SystemUiFlags.HideNavigation;
        //    uiOptions |= (int)SystemUiFlags.ImmersiveSticky;

        //    Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;

        //}

        //protected override void OnResume()
        //{
        //    base.OnResume();
        //    var uiOptions = (int)Window.DecorView.SystemUiVisibility;

        //    uiOptions |= (int)SystemUiFlags.LowProfile;
        //    uiOptions |= (int)SystemUiFlags.Fullscreen;
        //    uiOptions |= (int)SystemUiFlags.HideNavigation;
        //    uiOptions |= (int)SystemUiFlags.ImmersiveSticky;
        //    Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
        //}
    }
}

