﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using salesForce2.Droid.Render;
using salesForce2.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;



[assembly: ExportRenderer(typeof(NoUnderlineEditor), typeof(NoUnderlineEditorRenderer))]

namespace salesForce2.Droid.Render
{
    public class NoUnderlineEditorRenderer : EditorRenderer
    {
        [Preserve(AllMembers = true)]
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var gd = new GradientDrawable();
                gd.SetColor(global::Android.Graphics.Color.Transparent);
                Control.SetBackgroundDrawable(gd);
                //Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.SetHintTextColor(ColorStateList.ValueOf(global::Android.Graphics.Color.White));
            }

            if (e.NewElement != null)
            {
                var element = e.NewElement as NoUnderlineEditor;
                this.Control.Hint = element.Placeholder;
            }
        }

        protected override void OnElementPropertyChanged(object sender,PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == NoUnderlineEditor.PlaceholderProperty.PropertyName)
            {
                var element = this.Element as NoUnderlineEditor;
                this.Control.Hint = element.Placeholder;
            }
        }
    }
}
