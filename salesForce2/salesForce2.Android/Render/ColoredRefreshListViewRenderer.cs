﻿using System;
using Android.Support.V4.Widget;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform.Android;
using ListView = Xamarin.Forms.ListView;

namespace salesForce2.Droid.Render
{
    [Preserve(AllMembers = true)]

    public class ColoredRefreshListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            try
            {
                var x = Control.Parent as SwipeRefreshLayout;
                x?.SetColorSchemeColors(global::Android.Graphics.Color.ParseColor("#223c74"));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

    }
}
