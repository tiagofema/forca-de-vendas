﻿using Android.Graphics.Drawables;
using Android.Provider;
using Android.Views;
using Android.Widget;
using Java.Time.Format;
using salesForce2.Droid.Render;
using salesForce2.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;
using PickerRenderer = Xamarin.Forms.Platform.Android.AppCompat.PickerRenderer;


[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace salesForce2.Droid.Render
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;
            //Centraliza o texto

            Control.Gravity = GravityFlags.CenterHorizontal;
            //Remove as linhas inferiores
            Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            Control.SetTextColor(Color.Black);
            
            //GradientDrawable gd = new GradientDrawable();
            //gd.SetStroke(0, Android.Graphics.Color.Transparent);
            //Control.SetBackground(gd);
        }
    }
}